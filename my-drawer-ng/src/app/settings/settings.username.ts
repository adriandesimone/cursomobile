import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, ApplicationSettings } from "@nativescript/core";
import { Router } from "@angular/router";
import { RouterExtensions } from "@nativescript/angular";

@Component({
    selector: "Username",
    moduleId: module.id,
    templateUrl: "./settings.username.html"
})
export class SettingsUsername implements OnInit {
    textFieldValue: string = "";
    constructor(private router: Router, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        if( ApplicationSettings.hasKey("nombreUsuario") ) {
            this.textFieldValue = ApplicationSettings.getString("nombreUsuario");
        }
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.closeDrawer();
    }

    onButtonTap(){
        this.guardarUserName(this.textFieldValue);
    }

    guardarUserName(s: string){
        //console.log("guardarUserName: " + s);
        ApplicationSettings.setString("nombreUsuario", s);
        this.onNavItemTap('/settings');
    }
}
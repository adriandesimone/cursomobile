import { Injectable } from "@angular/core";
import { getJSON, request } from "@nativescript/core/http";
const sqlite = require("nativescript-sqlite");

@Injectable()
export class NoticiasService {
    api: string = "http://localhost:3000/"; //https://a7de86d08f62.ngrok.io";

    constructor() {
        this.getDB((db) => {
            console.dir(db);
            db.each("select * from logs",
                (err, fila) => console.log("fila: ", fila),
                (err, totales) => console.log("Filas totales: ", totales));
            }, () => console.log("error on getDB"));
    }

    getDB(fnOK, fnError){
        return new sqlite("mi_db_logs", (err, db) => {
            if (err) {
                console.error("Error al abrir la db!", err);
            }
            else {
                console.log("Está la db abierta: ", db.isOpen() ? "Si" : "No");
                db.execSQL("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                    .then( (id) => {
                        console.log("CREATE TABLE OK");
                        fnOK(db);
                    }, (error) => {
                        console.log("CREATE TABLE ERROR", error);
                        fnError(error);
                    });
            }
        });
    }

    agregar(s: string) {
        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify({
                nuevo: s
            })
        });
    }

    favs() {
        return getJSON(this.api + "/favs");
    }

    buscar(s: string) {
        this.getDB((db) => {
            db.execSQL("INSERT INTO logs (texto) VALUES (?)", [s],
                (err, id) => console.log("nuevo id: ", id));
            }, () => console.log("error on getDB"));

        return getJSON(this.api + "/get?q=" + s);
    }
}
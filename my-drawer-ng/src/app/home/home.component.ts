import { Component, OnInit } from "@angular/core";
import * as camera from "nativescript-camera";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, isAndroid, isIOS, Device, Screen } from "@nativescript/core";
import { Image } from "@nativescript/core";
//import * as imageSource from "image/source";
import { connectionType, getConnectionType, startMonitoring, stopMonitoring } from "@nativescript/core/connectivity"; 

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {
    onlyInAndroid: string;
    constructor() {
        // Use the component constructor to inject providers.
        this.onlyInAndroid = "<!-- Page content goes here -->";
    }

    ngOnInit(): void {
        // Init your component properties here.
        
        if(isAndroid)
            this.onlyInAndroid = "Is running in android";
        else
            this.onlyInAndroid = "It's not running in android";
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onButtonTap(): void {
        /*
        camera.requestPermissions().then(
            function success() {
                const options = { width: 300, height: 300, keepAspectRatio: false, saveToGallery: true };
                camera.takePicture(options).
                    then((imageAsset) => {
                        console.log("Tamaño: " + imageAsset.options.width + " x " + imageAsset.options.height);
                        console.log("keepAspectRatio: " + imageAsset.options.keepAspectRatio);
                        console.log("Foto Guardada!");
                    }).catch((err) => {
                        console.log("Error: " + err.message);
                    }
                );
            },
            function failure() {
                console.log("Permiso de cámara no aceptado por el usuario");
            }
        );
        */
       console.log("modelo", Device.model);
       console.log("tipo dispositivo", Device.deviceType);
       console.log("Sistema operativo", Device.os);
       console.log("versión sist operativo", Device.osVersion);
       console.log("Versión sdk", Device.sdkVersion);
       console.log("lenguaje", Device.language);
       console.log("fabricante", Device.manufacturer);
       console.log("código único de dispositivo", Device.uuid);
       console.log("altura en pixels normalizados", Screen.mainScreen.heightDIPs); // DIP (Device Independent Pixel), también conocido como densidad de píxeles independientes. Un píxel virtual que aparece aproximadamente del mismo tamaño en una variedad de densidades de pantalla.
       console.log("altura pixels", Screen.mainScreen.heightPixels);
       console.log("escala pantalla", Screen.mainScreen.scale);
       console.log("ancho pixels normalizados", Screen.mainScreen.widthDIPs);
       console.log("ancho pixels", Screen.mainScreen.widthPixels);
    }
}

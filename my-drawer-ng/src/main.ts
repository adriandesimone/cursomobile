// this import should be first in order to load some required settings (like globals and reflect-metadata)

import { platformNativeScriptDynamic, registerElement } from "@nativescript/angular";

import { AppModule } from "./app/app.module";

registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);

platformNativeScriptDynamic().bootstrapModule(AppModule);
